package com.test.spark.wiki.utils

import org.scalatest.FunSuite

class UtilsTest extends FunSuite {

  test("testToDecade 1992 -> 199X") {
    //G
    val year = 1992
    //E
    val expectedDecade = "199X"
    //W
    val result = Utils.toDecade(year)
    //T
    assert(result == expectedDecade)
  }


  test("testToDecade 1997 -> 199X") {
    //G
    val year = 1997
    //E
    val expectedDecade = "199X"
    //W
    val result = Utils.toDecade(year)
    //T
    assert(result == expectedDecade)
  }



  test("testToDecade 2006 -> 200X") {
    //G
    val year = 2006
    //E
    val expectedDecade = "200X"
    //W
    val result = Utils.toDecade(year)
    //T
    assert(result == expectedDecade)
  }

  test("testToDecade 2011 -> 201X") {
    //G
    val year = 2011
    //E
    val expectedDecade = "201X"
    //W
    val result = Utils.toDecade(year)
    //T
    assert(result == expectedDecade)
  }

}
