package com.test.spark.wiki.parser

import com.test.spark.wiki.domain.Domain.{LeagueStanding, Season}
import org.scalatest.FunSuite
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.NodeFilter


class LeaguesParserTest extends FunSuite {

  test("testParseWiki Ligue1") {
      // Given
      val html =
        """
          |<html>
          |
          |<body>
          |<div>
          |
          | <table class="wikitable gauche" style="text-align:center; line-height:16px;">
          |<caption>Classement
          |</caption>
          |<tbody><tr bgcolor="#F2F2F2">
          |<th scope="col" style="">Rang
          |</th>
          |<th scope="col" style="" width="200">Équipe
          |</th>
          |<th scope="col" style="width:20px"><span style="cursor:help;" title="Points">Pts</span>
          |</th>
          |<th scope="col" style="width:20px"><span style="cursor:help;" title="Matchs joués">J</span>
          |</th>
          |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Matchs gagnés">G</span>
          |</th>
          |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Matchs nuls">N</span>
          |</th>
          |<th scope="col" style="width:20px"><span style="cursor:help;" title="Matchs perdus">P</span>
          |</th>
          |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Buts pour">Bp</span>
          |</th>
          |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Buts contre">Bc</span>
          |</th>
          |<th scope="col" style="width:25px"><span style="cursor:help;" title="Différence de buts">Diff</span>
          |</th></tr>
          |<tr bgcolor="#97DEFF">
          |<td><b><span class="nowrap">1</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Lille_OSC" class="mw-redirect" title="Lille OSC">Lille OSC</a> <b><abbr class="abbr" title="Vainqueur de la coupe nationale"><sup>C</sup></abbr></b></span>
          |</td>
          |<td><b>76</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">21
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>4
          |</td>
          |<td style="border-right-style:hidden">68
          |</td>
          |<td style="border-right-style:hidden">36</td>
          |<td>+32
          |</td></tr>
          |<tr bgcolor="#97DEFF">
          |<td><b><span class="nowrap">2</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Olympique_de_Marseille" title="Olympique de Marseille">Olympique de Marseille</a> <b><abbr class="abbr" title="Tenant du titre"><sup>T</sup></abbr></b> <b><abbr class="abbr" title="Vainqueur de la coupe de la Ligue"><sup>L</sup></abbr></b> <b><abbr class="abbr" title="Vainqueur de la supercoupe nationale"><sup>S</sup></abbr></b></span>
          |</td>
          |<td><b>68</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">18
          |</td>
          |<td style="border-right-style:hidden">14
          |</td>
          |<td>6
          |</td>
          |<td style="border-right-style:hidden">62
          |</td>
          |<td style="border-right-style:hidden">39</td>
          |<td>+23
          |</td></tr>
          |<tr bgcolor="#CEEBFB">
          |<td><b><span class="nowrap">3</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Olympique_lyonnais" title="Olympique lyonnais">Olympique lyonnais</a></span>
          |</td>
          |<td><b>64</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">17
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>8
          |</td>
          |<td style="border-right-style:hidden">61
          |</td>
          |<td style="border-right-style:hidden">40</td>
          |<td>+21
          |</td></tr>
          |<tr bgcolor="#FFE052">
          |<td><b><span class="nowrap">4</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Paris_Saint-Germain" class="mw-redirect" title="Paris Saint-Germain">Paris Saint-Germain</a></span>
          |</td>
          |<td><b>60</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">15
          |</td>
          |<td style="border-right-style:hidden">15
          |</td>
          |<td>8
          |</td>
          |<td style="border-right-style:hidden">56
          |</td>
          |<td style="border-right-style:hidden">41</td>
          |<td>+15
          |</td></tr>
          |<tr bgcolor="#FFE052">
          |<td><b><span class="nowrap">5</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/FC_Sochaux-Montb%C3%A9liard" class="mw-redirect" title="FC Sochaux-Montbéliard">FC Sochaux-Montbéliard</a></span>
          |</td>
          |<td><b>58</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">17
          |</td>
          |<td style="border-right-style:hidden">7
          |</td>
          |<td>14
          |</td>
          |<td style="border-right-style:hidden">60
          |</td>
          |<td style="border-right-style:hidden">43</td>
          |<td>+17
          |</td></tr>
          |<tr bgcolor="#FFF052">
          |<td><b><span class="nowrap">6</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Stade_rennais_FC" class="mw-redirect" title="Stade rennais FC">Stade rennais FC</a></span>
          |</td>
          |<td><b>56</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">15
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td>12
          |</td>
          |<td style="border-right-style:hidden">38
          |</td>
          |<td style="border-right-style:hidden">35</td>
          |<td>+3
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">7</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/FC_Girondins_de_Bordeaux" class="mw-redirect" title="FC Girondins de Bordeaux">FC Girondins de Bordeaux</a></span>
          |</td>
          |<td><b>51</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">12
          |</td>
          |<td style="border-right-style:hidden">15
          |</td>
          |<td>11
          |</td>
          |<td style="border-right-style:hidden">43
          |</td>
          |<td style="border-right-style:hidden">42</td>
          |<td>+1
          |</td></tr>
          |<tr bgcolor="#F9F9F9">
          |<td><b><span class="nowrap">8</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Toulouse_FC" class="mw-redirect" title="Toulouse FC">Toulouse FC</a></span>
          |</td>
          |<td><b>50</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">14
          |</td>
          |<td style="border-right-style:hidden">8
          |</td>
          |<td>16
          |</td>
          |<td style="border-right-style:hidden">38
          |</td>
          |<td style="border-right-style:hidden">36</td>
          |<td>+2
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">9</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/AJ_Auxerre" class="mw-redirect" title="AJ Auxerre">AJ Auxerre</a></span>
          |</td>
          |<td><b>49</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">10
          |</td>
          |<td style="border-right-style:hidden">19
          |</td>
          |<td>9
          |</td>
          |<td style="border-right-style:hidden">45
          |</td>
          |<td style="border-right-style:hidden">41</td>
          |<td>+4
          |</td></tr>
          |<tr bgcolor="#F9F9F9">
          |<td><b><span class="nowrap">10</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/AS_Saint-%C3%89tienne" class="mw-redirect" title="AS Saint-Étienne">AS Saint-Étienne</a></span>
          |</td>
          |<td><b>49</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">12
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>13
          |</td>
          |<td style="border-right-style:hidden">46
          |</td>
          |<td style="border-right-style:hidden">47</td>
          |<td>-1
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">11</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/FC_Lorient" class="mw-redirect" title="FC Lorient">FC Lorient</a></span>
          |</td>
          |<td><b>49</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">12
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>13
          |</td>
          |<td style="border-right-style:hidden">46
          |</td>
          |<td style="border-right-style:hidden">48</td>
          |<td>-2
          |</td></tr>
          |<tr bgcolor="#F9F9F9">
          |<td><b><span class="nowrap">12</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Valenciennes_FC" class="mw-redirect" title="Valenciennes FC">Valenciennes FC</a></span>
          |</td>
          |<td><b>48</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">10
          |</td>
          |<td style="border-right-style:hidden">18
          |</td>
          |<td>10
          |</td>
          |<td style="border-right-style:hidden">45
          |</td>
          |<td style="border-right-style:hidden">41</td>
          |<td>+4
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">13</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/AS_Nancy-Lorraine" class="mw-redirect" title="AS Nancy-Lorraine">AS Nancy-Lorraine</a></span>
          |</td>
          |<td><b>48</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td style="border-right-style:hidden">9
          |</td>
          |<td>16
          |</td>
          |<td style="border-right-style:hidden">43
          |</td>
          |<td style="border-right-style:hidden">48</td>
          |<td>-5
          |</td></tr>
          |<tr bgcolor="#F9F9F9">
          |<td><b><span class="nowrap">14</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Montpellier_H%C3%A9rault_Sport_Club" title="Montpellier Hérault Sport Club">Montpellier HSC</a></span>
          |</td>
          |<td><b>47</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">12
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td>15
          |</td>
          |<td style="border-right-style:hidden">32
          |</td>
          |<td style="border-right-style:hidden">43</td>
          |<td>-11
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">15</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/SM_Caen" class="mw-redirect" title="SM Caen">SM Caen</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b></span>
          |</td>
          |<td><b>46</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>14
          |</td>
          |<td style="border-right-style:hidden">46
          |</td>
          |<td style="border-right-style:hidden">51</td>
          |<td>-5
          |</td></tr>
          |<tr bgcolor="#F9F9F9">
          |<td><b><span class="nowrap">16</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/Stade_brestois_29" title="Stade brestois 29">Stade brestois</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b></span>
          |</td>
          |<td><b>46</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>14
          |</td>
          |<td style="border-right-style:hidden">36
          |</td>
          |<td style="border-right-style:hidden">43</td>
          |<td>-7
          |</td></tr>
          |<tr bgcolor="white">
          |<td><b><span class="nowrap">17</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/OGC_Nice" class="mw-redirect" title="OGC Nice">OGC Nice</a></span>
          |</td>
          |<td><b>46</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td style="border-right-style:hidden">13
          |</td>
          |<td>14
          |</td>
          |<td style="border-right-style:hidden">33
          |</td>
          |<td style="border-right-style:hidden">48</td>
          |<td>-15
          |</td></tr>
          |<tr bgcolor="#FFCCCC">
          |<td><b><span class="nowrap">18</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/AS_Monaco_FC" class="mw-redirect" title="AS Monaco FC">AS Monaco FC</a></span>
          |</td>
          |<td><b>44</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">9
          |</td>
          |<td style="border-right-style:hidden">17
          |</td>
          |<td>12
          |</td>
          |<td style="border-right-style:hidden">36
          |</td>
          |<td style="border-right-style:hidden">40</td>
          |<td>-4
          |</td></tr>
          |<tr bgcolor="#FFCCCC">
          |<td><b><span class="nowrap">19</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/RC_Lens" class="mw-redirect" title="RC Lens">RC Lens</a></span>
          |</td>
          |<td><b>35</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">7
          |</td>
          |<td style="border-right-style:hidden">14
          |</td>
          |<td>17
          |</td>
          |<td style="border-right-style:hidden">35
          |</td>
          |<td style="border-right-style:hidden">58</td>
          |<td>-23
          |</td></tr>
          |<tr bgcolor="#FFCCCC">
          |<td><b><span class="nowrap">20</span></b>
          |</td>
          |<td align="left"><span class="nowrap"><a href="/wiki/AC_Arles-Avignon" class="mw-redirect" title="AC Arles-Avignon">AC Arles-Avignon</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b></span>
          |</td>
          |<td><b>20</b>
          |</td>
          |<td>38
          |</td>
          |<td style="border-right-style:hidden">3
          |</td>
          |<td style="border-right-style:hidden">11
          |</td>
          |<td>24
          |</td>
          |<td style="border-right-style:hidden">21
          |</td>
          |<td style="border-right-style:hidden">70</td>
          |<td>-49
          |</td></tr></tbody></table>
          |
          |</div>
          |</body>
          |</html>
        """.stripMargin
      val doc:Document = Jsoup.parseBodyFragment(html)
      val table = doc.select(LeaguesParser.SELECTOR).first()

      val season = Season(2010, "Ligue 1", "https://fr.wikipedia.org/wiki/Championnat_de_France_de_football_2010-2011")

    //Expected
    val champion = LeagueStanding("Ligue 1",2010,1,"Lille OSC",76,38,21,13,4,68,36,32)

    // When
     val list:Seq[LeagueStanding] = LeaguesParser.parseWiki(season,table)

    // Then
    assert(champion == list.head)
  }



  test("testParseWiki Ligue1 2013 seems not working") {
    // Given
    val html =
      """
        |<html>
        |
        |<body>
        |<div>
        |<table class="wikitable gauche" style="text-align:center; line-height:16px;">
        |<caption>Classement
        |</caption>
        |<tbody><tr bgcolor="#F2F2F2">
        |<th scope="col" style="">Rang
        |</th>
        |<th scope="col" style="" width="200">Équipe
        |</th>
        |<th scope="col" style="width:20px"><span style="cursor:help;" title="Points">Pts</span>
        |</th>
        |<th scope="col" style="width:20px"><span style="cursor:help;" title="Matchs joués">J</span>
        |</th>
        |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Matchs gagnés">G</span>
        |</th>
        |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Matchs nuls">N</span>
        |</th>
        |<th scope="col" style="width:20px"><span style="cursor:help;" title="Matchs perdus">P</span>
        |</th>
        |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Buts pour">Bp</span>
        |</th>
        |<th scope="col" style="width:20px;border-right-style:hidden"><span style="cursor:help;" title="Buts contre">Bc</span>
        |</th>
        |<th scope="col" style="width:25px"><span style="cursor:help;" title="Différence de buts">Diff</span>
        |</th></tr>
        |<tr bgcolor="#97DEFF">
        |<td><b><span class="nowrap">1</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><b><a href="/wiki/Paris_Saint-Germain_Football_Club" title="Paris Saint-Germain Football Club">Paris Saint-Germain</a></b> <b><abbr class="abbr" title="Tenant du titre"><sup>T</sup></abbr></b> <b><abbr class="abbr" title="Vainqueur de la supercoupe nationale"><sup>S</sup></abbr></b> <b><abbr class="abbr" title="Vainqueur de la coupe de la Ligue"><sup>L</sup></abbr></b></span>
        |</td>
        |<td><b>89</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">27
        |</td>
        |<td style="border-right-style:hidden">8
        |</td>
        |<td>3
        |</td>
        |<td style="border-right-style:hidden">84
        |</td>
        |<td style="border-right-style:hidden">23</td>
        |<td>+61
        |</td></tr>
        |<tr bgcolor="#97DEFF">
        |<td><b><span class="nowrap">2</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Association_sportive_de_Monaco_football_club" title="Association sportive de Monaco football club">AS Monaco FC</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b></span>
        |</td>
        |<td><b>80</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">23
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td>4
        |</td>
        |<td style="border-right-style:hidden">63
        |</td>
        |<td style="border-right-style:hidden">31</td>
        |<td>+32
        |</td></tr>
        |<tr bgcolor="#CEEBFB">
        |<td><b><span class="nowrap">3</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/LOSC_Lille" title="LOSC Lille">Lille OSC</a></span>
        |</td>
        |<td><b>71</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">20
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td>7
        |</td>
        |<td style="border-right-style:hidden">46
        |</td>
        |<td style="border-right-style:hidden">26</td>
        |<td>+20
        |</td></tr>
        |<tr bgcolor="#FFF052">
        |<td><b><span class="nowrap">4</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Association_sportive_de_Saint-%C3%89tienne" title="Association sportive de Saint-Étienne">AS Saint-Étienne</a></span>
        |</td>
        |<td><b>69</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">20
        |</td>
        |<td style="border-right-style:hidden">9
        |</td>
        |<td>9
        |</td>
        |<td style="border-right-style:hidden">56
        |</td>
        |<td style="border-right-style:hidden">34</td>
        |<td>+22
        |</td></tr>
        |<tr bgcolor="#FFF052">
        |<td><b><span class="nowrap">5</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Olympique_lyonnais" title="Olympique lyonnais">Olympique lyonnais</a></span>
        |</td>
        |<td><b>61</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">17
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td>11
        |</td>
        |<td style="border-right-style:hidden">56
        |</td>
        |<td style="border-right-style:hidden">44</td>
        |<td>+12
        |</td></tr>
        |<tr bgcolor="#F9F9F9">
        |<td><b><span class="nowrap">6</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Olympique_de_Marseille" title="Olympique de Marseille">Olympique de Marseille</a></span>
        |</td>
        |<td><b>60</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">16
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td>10
        |</td>
        |<td style="border-right-style:hidden">53
        |</td>
        |<td style="border-right-style:hidden">40</td>
        |<td>+13
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">7</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Football_Club_des_Girondins_de_Bordeaux" title="Football Club des Girondins de Bordeaux">FC Girondins de Bordeaux</a></span>
        |</td>
        |<td><b>53</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">13
        |</td>
        |<td style="border-right-style:hidden">14
        |</td>
        |<td>11
        |</td>
        |<td style="border-right-style:hidden">49
        |</td>
        |<td style="border-right-style:hidden">43</td>
        |<td>+6
        |</td></tr>
        |<tr bgcolor="#F9F9F9">
        |<td><b><span class="nowrap">8</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Football_Club_Lorient" title="Football Club Lorient">FC Lorient</a></span>
        |</td>
        |<td><b>49</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">13
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td>15
        |</td>
        |<td style="border-right-style:hidden">48
        |</td>
        |<td style="border-right-style:hidden">53</td>
        |<td>-5
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">9</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Toulouse_Football_Club" title="Toulouse Football Club">Toulouse FC</a></span>
        |</td>
        |<td><b>49</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td style="border-right-style:hidden">13
        |</td>
        |<td>13
        |</td>
        |<td style="border-right-style:hidden">46
        |</td>
        |<td style="border-right-style:hidden">53</td>
        |<td>-7
        |</td></tr>
        |<tr bgcolor="#F9F9F9">
        |<td><b><span class="nowrap">10</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Sporting_Club_de_Bastia" class="mw-redirect" title="Sporting Club de Bastia">SC Bastia</a></span>
        |</td>
        |<td><b>49</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">13
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td>15
        |</td>
        |<td style="border-right-style:hidden">42
        |</td>
        |<td style="border-right-style:hidden">56</td>
        |<td>-14
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">11</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Stade_de_Reims" title="Stade de Reims">Stade de Reims</a></span>
        |</td>
        |<td><b>48</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td>14
        |</td>
        |<td style="border-right-style:hidden">44
        |</td>
        |<td style="border-right-style:hidden">52</td>
        |<td>-8
        |</td></tr>
        |<tr bgcolor="#F9F9F9">
        |<td><b><span class="nowrap">12</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Stade_rennais_football_club" title="Stade rennais football club">Stade rennais FC</a></span>
        |</td>
        |<td><b>46</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td style="border-right-style:hidden">13
        |</td>
        |<td>14
        |</td>
        |<td style="border-right-style:hidden">47
        |</td>
        |<td style="border-right-style:hidden">45</td>
        |<td>+2
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">13</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Football_Club_de_Nantes" title="Football Club de Nantes">FC Nantes</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b></span>
        |</td>
        |<td><b>46<sup><a href="#A">A</a></sup></b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td>16
        |</td>
        |<td style="border-right-style:hidden">38
        |</td>
        |<td style="border-right-style:hidden">43</td>
        |<td>-5
        |</td></tr>
        |<tr bgcolor="#F9F9F9">
        |<td><b><span class="nowrap">14</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/%C3%89vian_Thonon_Gaillard_Football_Club" class="mw-redirect" title="Évian Thonon Gaillard Football Club">Évian TG FC</a></span>
        |</td>
        |<td><b>44</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td>16
        |</td>
        |<td style="border-right-style:hidden">39
        |</td>
        |<td style="border-right-style:hidden">51</td>
        |<td>-12
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">15</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Montpellier_H%C3%A9rault_Sport_Club" title="Montpellier Hérault Sport Club">Montpellier HSC</a></span>
        |</td>
        |<td><b>42</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">8
        |</td>
        |<td style="border-right-style:hidden">18
        |</td>
        |<td>12
        |</td>
        |<td style="border-right-style:hidden">45
        |</td>
        |<td style="border-right-style:hidden">53</td>
        |<td>-8
        |</td></tr>
        |<tr bgcolor="#FFE052">
        |<td><b><span class="nowrap">16</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/En_Avant_de_Guingamp" class="mw-redirect" title="En Avant de Guingamp">EA Guingamp</a> <b><abbr class="abbr" title="Promu"><sup>P</sup></abbr></b> <b><abbr class="abbr" title="Vainqueur de la coupe nationale"><sup>C</sup></abbr></b></span>
        |</td>
        |<td><b>42</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td style="border-right-style:hidden">9
        |</td>
        |<td>18
        |</td>
        |<td style="border-right-style:hidden">34
        |</td>
        |<td style="border-right-style:hidden">42</td>
        |<td>-8
        |</td></tr>
        |<tr bgcolor="white">
        |<td><b><span class="nowrap">17</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Olympique_Gymnaste_Club_Nice_C%C3%B4te_d%27Azur" class="mw-redirect" title="Olympique Gymnaste Club Nice Côte d'Azur">OGC Nice</a></span>
        |</td>
        |<td><b>42</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">12
        |</td>
        |<td style="border-right-style:hidden">6
        |</td>
        |<td>20
        |</td>
        |<td style="border-right-style:hidden">30
        |</td>
        |<td style="border-right-style:hidden">44</td>
        |<td>-14
        |</td></tr>
        |<tr bgcolor="#FFCCCC">
        |<td><b><span class="nowrap">18</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Football_Club_Sochaux-Montb%C3%A9liard" title="Football Club Sochaux-Montbéliard">FC Sochaux-Montbéliard</a></span>
        |</td>
        |<td><b>40</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td style="border-right-style:hidden">10
        |</td>
        |<td>18
        |</td>
        |<td style="border-right-style:hidden">37
        |</td>
        |<td style="border-right-style:hidden">61</td>
        |<td>-24
        |</td></tr>
        |<tr bgcolor="#FFCCCC">
        |<td><b><span class="nowrap">19</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Valenciennes_Football_Club" title="Valenciennes Football Club">Valenciennes FC</a></span>
        |</td>
        |<td><b>29</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">7
        |</td>
        |<td style="border-right-style:hidden">8
        |</td>
        |<td>23
        |</td>
        |<td style="border-right-style:hidden">37
        |</td>
        |<td style="border-right-style:hidden">65</td>
        |<td>-28
        |</td></tr>
        |<tr bgcolor="#FFCCCC">
        |<td><b><span class="nowrap">20</span></b>
        |</td>
        |<td align="left"><span class="nowrap"><a href="/wiki/Athletic_club_ajaccien" class="mw-redirect" title="Athletic club ajaccien">AC Ajaccio</a></span>
        |</td>
        |<td><b>23</b>
        |</td>
        |<td>38
        |</td>
        |<td style="border-right-style:hidden">4
        |</td>
        |<td style="border-right-style:hidden">11
        |</td>
        |<td>23
        |</td>
        |<td style="border-right-style:hidden">37
        |</td>
        |<td style="border-right-style:hidden">72</td>
        |<td>-35
        |</td></tr></tbody></table>
        |</div>
        |</body>
        |</html>
      """.stripMargin
    val doc:Document = Jsoup.parseBodyFragment(html)
    val table = doc.select(LeaguesParser.SELECTOR).first()

    val season = Season(2013, "Ligue 1", "https://fr.wikipedia.org/wiki/Championnat_de_France_de_football_2013-2014")

    //Expected
    val champion = LeagueStanding("Ligue 1",2013,1,"Paris Saint-Germain",89,38,27,8,3,84,23,61)
    // When
    val list:Seq[LeagueStanding] = LeaguesParser.parseWiki(season, table)

    // Then
    assert(champion == list.head)
  }


  test("testParseWiki Ligue1 1998 seems not working") {
   //G
    val season = Season(1997, "Ligue 1", "https://fr.wikipedia.org/wiki/Championnat_de_France_de_football_1997-1998")
    val doc = Jsoup.connect(season.url).get()
    val tables = doc.select(LeaguesParser.SELECTOR)
    println(s"Tables  ==> ${tables}")
  }





}
