package com.test.spark.wiki.parser

import com.test.spark.wiki.domain.Domain._
import com.test.spark.wiki.domain.Domain.LeagueStanding
import org.jsoup.nodes.Element
import scala.collection.JavaConversions._


object LeaguesParser {

  val SELECTOR = "table.wikitable.gauche"

  def parseWiki(season: Season, table: Element): Seq[LeagueStanding] = {

    //take Tail of list (skip the first line = HEADER)
    val rows = table.select("tr").toArray.tail
    rows
      .map{
        case row: Element => {
          val cols = row.select("td")

          //Delete the "Sup" element inside POINTS col
          cols(2).select("b").select("sup").remove()

          new LeagueStanding(

            league = season.name,
            season = season.season,

            //TODO: should verify if cols size == 10 ???!!!!
            position = cols(0).text().toInt,
            team = cols(1).select("a").first().text(),
            points = cols(2).select("b").text().toInt,
            played = cols(3).text().toInt,
            won = cols(4).text().toInt,
            drawn = cols(5).text().toInt,
            lost = cols(6).text().toInt,
            goalsFor = cols(7).text().toInt,
            goalsAgainst = cols(8).text().toInt,
            goalsDifference = cols(9).text().toInt
          )
        }

    }.toSeq

  }


}
