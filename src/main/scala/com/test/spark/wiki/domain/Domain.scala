package com.test.spark.wiki.domain

import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.spark.sql.Encoders

object Domain {

  case class LeagueInput(@JsonProperty name: String, @JsonProperty url: String)

  case class LeagueStanding(league: String,
                            season: Int,
                            position: Int,
                            team: String,
                            points: Int,
                            played: Int,
                            won: Int,
                            drawn: Int,
                            lost: Int,
                            goalsFor: Int,
                            goalsAgainst: Int,
                            goalsDifference: Int)

  case class Season(season: Int, name: String, url :String)



  implicit val encoderLeagues = Encoders.product[LeagueInput]
  implicit val encoderLeagueStanding = Encoders.product[LeagueStanding]
  implicit val encoderSeason = Encoders.product[Season]

}
