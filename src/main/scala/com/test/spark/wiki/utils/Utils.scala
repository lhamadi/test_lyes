package com.test.spark.wiki.utils

object Utils {
  def toDecade(year: Int): String = {
    s"${Math.round(year/10)}X"
  }
}
