package com.test.spark.wiki.extracts


import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.test.spark.wiki.parser.LeaguesParser
import com.test.spark.wiki.domain.Domain._
import com.test.spark.wiki.domain.Domain.{LeagueInput, LeagueStanding}
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import org.joda.time.DateTime
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.{Logger, LoggerFactory}

case class Q1_WikiDocumentsToParquetTask(bucket: String) extends Runnable {
  private val session: SparkSession = SparkSession.builder().getOrCreate()
  private val logger: Logger = LoggerFactory.getLogger(getClass)


  override def run(): Unit = {
    val toDate = DateTime.now().withYear(2017)
    val fromDate = toDate.minusYears(20)

    // get Seq of IL
    val leaguesSeq = getLeagues

    // Create DataSet from Seq
    val leaguesDS:Dataset[LeagueInput] = session.createDataset(leaguesSeq)

    // I prefer creating Season case class to better manage "seasons" instead of tuple, also for catalyst optim
    val seasonDS: Dataset[Season] = leaguesDS
      .flatMap {
        input =>
          (fromDate.getYear until toDate.getYear).map {
            year =>
              Season(year + 1 , input.name, input.url.format(year, year + 1))
          }
      }

    val orderDS: Dataset[LeagueStanding] = seasonDS.
      flatMap {
        case currentSeason:Season =>
          try {
            // ATTENTION:
            //  - Quelques pages ne respectent pas le format commun et pourraient planter - pas grave
            val doc:Element = Jsoup.connect(currentSeason.url).get().body()
            //  - Il faut veiller à recuperer uniquement le classement général
            val table = doc.select(LeaguesParser.SELECTOR).first()

            //  - Il faut normaliser les colonnes "positions", "teams" et "points" en cas de problèmes de formatage
            //TODO: faire les Test Unitaire et traiter les cas de mauvais formatage
            LeaguesParser.parseWiki(currentSeason, table)

          } catch {
            case _: Throwable =>
              // TODO Q3 En supposant que ce job tourne sur un cluster EMR, où seront affichés les logs d'erreurs ?
              logger.warn(s"Can't parse season ${currentSeason.season} from ${currentSeason.url}")
              Seq.empty
          }
    }

    orderDS
      // TODO Q4 Comment partitionner les données en 2 avant l'écriture sur le bucket
      .coalesce(2)  // We can also use Coalesce, because default partition = 200, so from 200 -> 2 partition
      .write
      .mode(SaveMode.Overwrite)
      .parquet(bucket)

    // TODO Q5 Quel est l'avantage du format parquet par rapport aux autres formats ?
    /*
    le format parquet est un format binaire, orienté "column" c-a-d il maintient un système d'indexation des données de chaque
    colonne, d'ou l'avantage de requeter par prédicat sur les colone, car il évite un fullscan (comme CSV etc...)
    le gain en stokage est conséquent, de mon expérience, c'est au moins x10 notamment si bcp de données chaines de caractère
    et référentielle (exemple Currency EUR qui se repète sur toute la table de 1 milliards de lignes parquet par nature la compresse)
     */

    // TODO Q6 Quel est l'avantage de passer d'une séquence scala à un dataset spark ?
    /*
    le seul avantage dans notre exercice est le fait de paralléliser les calculs en mode distribué, plutôt
    que de les paralléliser en local, aucun enjeux de volume, simplement celui de // les calculs sur plusieurs Executors
     */
  }

  private def getLeagues: Seq[LeagueInput] = {
    val mapper = new ObjectMapper(new YAMLFactory()) // with ScalaObjectMapper
    // TODO Q7 Recuperer l'input stream du fichier leagues.yaml
    mapper.registerModule(DefaultScalaModule)
    // create stream from leagues.yml resource
    val inputStream = getClass().getClassLoader().getResourceAsStream("leagues.yaml")
    mapper.readValue(inputStream, classOf[Array[LeagueInput]]).toSeq
  }
}

// TODO Q8 Ajouter les annotations manquantes pour pouvoir mapper le fichier yaml à cette classe
// j'ai déplacé les case class dans un package dédié, pour plus de lisibilité,
// en plus de rajouter les implicits de leur encoders dans la même place