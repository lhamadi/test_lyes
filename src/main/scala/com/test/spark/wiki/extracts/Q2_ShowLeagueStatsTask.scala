package com.test.spark.wiki.extracts

import com.test.spark.wiki.domain.Domain._
import com.test.spark.wiki.utils.Utils
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions._

case class Q2_ShowLeagueStatsTask(bucket: String) extends Runnable {
  private val session: SparkSession = SparkSession.builder().getOrCreate()

  import session.implicits._

  override def run(): Unit = {
    val standings = session.read.parquet(bucket).as[LeagueStanding].cache()

    // TODO Répondre aux questions suivantes en utilisant le dataset $standings
    standings
      // ...code...
      .show()



    standings.createTempView("standings")


    // TODO Q1
    val mean_goals_query =
      """
        |
        |select league, season, avg(goalsFor) as avg_goalsFor
        |from standings
        |group by league, season
        |order by avg_goalsFor desc
        |
      """.stripMargin
    val meanGoalsDF = session.sql(mean_goals_query)
    println("Utiliser createTempView sur $standings et, en sql, afficher la moyenne de buts par saison et " +
      "par championnat")
    println("la moyenne par saison est toujours liée à une ligue, donc on affiche la ligue/saison/moyenne")
    meanGoalsDF.show()

    // TODO Q2
    val best_query =
      """
        |select league, team, count(*) as champion from standings
        |where league = 'Ligue 1' and position = 1
        |group by league, team
        |limit 1
      """.stripMargin
    val bestTeamDF = session.sql(best_query)

    println("En Dataset, quelle est l'équipe la plus titrée de France ?")
    bestTeamDF.show()


    // TODO Q3
    val mean_points_champions_query =
      """
        |select league, avg(points) as mean_points from standings
        |where  position = 1
        |group by league
        |order by mean_points desc
      """.stripMargin
    println("En Dataset, quelle est la moyenne de points des vainqueurs sur les 5 différents championnats ?")
    val meanPointsChampionsByLeagueF = session.sql(mean_points_champions_query)
    meanPointsChampionsByLeagueF.show()

    // TODO Q5 Ecrire une udf spark "decade" qui retourne la décennie d'une saison sous la forme 199X ?
    // see UtilsTest tests
    println(" UDF DECADE")

    val decadeUDF = Utils.toDecade(_)
    session.udf.register("to_decade", decadeUDF)

    val decade_query =
      """
        |select *, to_decade(season) as decade
        |from standings
        |
       """.stripMargin

    val decadeDF = session.sql(decade_query)
    decadeDF.show()

    // TODO Q4

    val sub_query_delta =
      """
        |select
        |
        |   league,
        |   to_decade(season) as decade,
        |   team,
        |   position,
        |   points,
        |   max(points) over (partition by season, league) - points  as delta_points
        |
        |from standings
      """.stripMargin

    val delta_1_10_query =
      s"""
        |
        |select league, decade, avg(delta_points) as avg_delta_points
        |
        |from ( ${sub_query_delta} ) as table
        |
        |where table.position = 10
        |
        |group by league, decade
        |
        |order by avg_delta_points desc
        |
      """.stripMargin
    println("En Dataset, quelle est la moyenne de points d'écart entre le 1er et le 10ème de chaque championnat" +
      "par décennie")
    val meanDeltaBtwOneAndTenDF = session.sql(delta_1_10_query)
    meanDeltaBtwOneAndTenDF.show(100)


  }
}
